/**
* @api {post} /auth/login Login
* @apiVersion 0.0.1
* @apiName Zaloex owner login
* @apiGroup Auth
*
* @apiParam {String} username car owner email.
* @apiParam {String} password car owner password.
*
* @apiHeader {String} Content-Type content type of the returned
* @apiHeaderExample {json} Header-Accept:
*     {
*       "Content-Type": " application/json"
*     }
*
* @apiSuccess {String} status_code status code.
* @apiSuccess {String} message response message.
* @apiSuccess {Object} data response data.
*
* @apiSampleRequest /auth/login
* @apiSuccessExample {json} Success-Response:
* {
  "status_code": 200,
  "message": "Success",
  "data": {
    "JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzY29wZXMiOlsiYWxsIl0sImp0aSI6IjNGT21lN1Zpa2l3V0ZScnBvYlg0YjJobEQ0aTc3Z08yWEhFT1hodkMwaU9KNnp2WEZJTlVvYlVTUWlnRUc1SUciLCJpYXQiOjE0Njk4NjAwNjUsImlzcyI6ImhvbmRhLXByb2plY3QiLCJzdWIiOiI1Nzk3MzY3N2FmZTZlODBjODYyMzhiM2YifQ.VLjOqnPNFdjkn960eueIgF8m5FmcsCNrot-GP202-5Q",
    "_id": "57973677afe6e80c86238b3f",
    "email": "x@x.com",
    "fullName": "Daniel",
    "phone": "123456",
    "role": "userRole"
  }
}
*/

/**
 * @api {post} /auth/logout logout
 * @apiVersion 0.0.1
 * @apiName owner logout
 * @apiGroup Auth
 *
 * @apiHeader {String} Content-Type content type of the returned
 * @apiHeaderExample {json} Header-Accept:
 *     {
*       "Content-Type": " application/json"
*     }
 *
 * @apiSuccess {String} status_code status code.
 * @apiSuccess {String} message response message.
 * @apiSuccess {Object} data response data.
 *
 * @apiSampleRequest /auth/logout
 * @apiSuccessExample {json} Success-Response:
 * {
  "status_code": 200,
  "message": "Success",
  "data": {
  }
}
 */
