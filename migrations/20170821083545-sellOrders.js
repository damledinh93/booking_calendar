'use strict';

const table = 'SellOrders';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(table, {
            id: { type: Sequelize.STRING, allowNull: false, unique: true, primaryKey: true },
            status : { type: Sequelize.STRING, allowNull: false, defaultValue: 'pending' },
            userId: {
                type: Sequelize.STRING,
                references: {
                    model: 'Users',
                    key: 'id'
                }
            },
            coinCurrency: { type: Sequelize.STRING },
            currency: { type: Sequelize.STRING },
            rate: { type: Sequelize.STRING(32) },
            volumeLimitMin: { type: Sequelize.STRING(32) },
            volumeLimitMax: { type: Sequelize.STRING(32) },
            priceLimitMax: { type: Sequelize.STRING(32) },
            feeCreateOrder: { type: Sequelize.STRING(32) },
            paymentType: { type: Sequelize.STRING },
            paymentInfo: { type: Sequelize.JSON },
            exchange: { type: Sequelize.STRING },
            timeout : { type: Sequelize.INTEGER },
            createdAt: { type: Sequelize.DATE },
            updatedAt: { type: Sequelize.DATE }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable(table);
    }
};
