'use strict';

const table = 'Users';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(table, 'token_password', { type: Sequelize.UUID });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(table, 'token_password');
  }
};
