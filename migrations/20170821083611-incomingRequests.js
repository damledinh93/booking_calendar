'use strict';

const table = 'IncomingRequests';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(table, {
            id: { type: Sequelize.STRING, allowNull: false, unique: true, primaryKey: true },
            status: { type: Sequelize.STRING, allowNull: false, defaultVallue: 'pending' },
            userId: {
                type: Sequelize.STRING,
                references: {
                    model: 'Users',
                    key: 'id'
                }
            },
            coinAmount: { type: Sequelize.STRING(32), allowNull: false },
            coinCurrency: { type: Sequelize.STRING, allowNull: false },
            txid: { type: Sequelize.STRING, allowNull: false },
            toAddress: { type: Sequelize.STRING, allowNull: false },
            createdAt: { type: Sequelize.DATE },
            updatedAt: { type: Sequelize.DATE }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable(table);
    }
};
