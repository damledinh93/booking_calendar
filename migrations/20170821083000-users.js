'use strict';

const table = 'Users';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(table, {
            id: { type: Sequelize.STRING, primaryKey: true, unique: true },
            username: { type: Sequelize.STRING, unique: true, allowNull: false },
            password: { type: Sequelize.STRING, allowNull: false },
            type: { type: Sequelize.STRING, defaultValue: 'user' },
            avatar: { type: Sequelize.STRING },
            wallets: { type: Sequelize.ARRAY(Sequelize.JSON) },
            phone: { type: Sequelize.STRING },
            phone_verified: { type: Sequelize.BOOLEAN, defaultValue: false },
            phone_verification_code: { type: Sequelize.INTEGER, allowNull: false },
            email: { type: Sequelize.STRING },
            email_verified: { type: Sequelize.BOOLEAN, defaultValue: false },
            email_verification_code: { type: Sequelize.INTEGER, allowNull: false },
            info: {type: Sequelize.JSON },
            createdAt: { type: Sequelize.DATE, defaultValue: new Date() },
            updatedAt: { type: Sequelize.DATE, defaultValue: new Date() }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable(table);
    }
};
