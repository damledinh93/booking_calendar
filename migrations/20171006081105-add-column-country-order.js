'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return [
			queryInterface.addColumn(
				'BuyOrders',
				'country_code',
				{
					type: Sequelize.STRING,
					defaultValue: 'SG'
				}
			),
			queryInterface.addColumn(
				'SellOrders',
				'country_code',
				{
					type: Sequelize.STRING,
					defaultValue: 'SG'
				}
			)
		];
	},

	down: (queryInterface, Sequelize) => {
		return [
			queryInterface.removeColumn('BuyOrders', 'country_code'),
			queryInterface.removeColumn('SellOrders', 'country_code')
		];
	}
};
