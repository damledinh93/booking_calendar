'use strict';

const table = 'BuyOrders';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(table, 'approval', {
			type: Sequelize.STRING, defaultValue: 'hold' 
		});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(table, 'approval');
  }
};
