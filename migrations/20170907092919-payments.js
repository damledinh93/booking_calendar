'use strict';

const tableName = 'Payments';
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(tableName, {
      id: { type: Sequelize.STRING, primaryKey: true, unique: true },
      buyRequestId: {
        type: Sequelize.STRING,
        references: {
          model: 'BuyRequests',
          key: 'id'
        }
      },
      sellRequestId: {
        type: Sequelize.STRING,
        references: {
          model: 'SellRequests',
          key: 'id'
        }
      },
      type: { type: Sequelize.STRING, defaultValue: 'bank' },
      info: { type: Sequelize.JSON },
      createdAt: { type: Sequelize.DATE, defaultValue: new Date() },
      updatedAt: { type: Sequelize.DATE, defaultValue: new Date() }
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable(tableName);
  }
};
