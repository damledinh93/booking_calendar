'use strict';

const table = 'SellOrders';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(table, 'approval', {
			type: Sequelize.STRING, defaultValue: 'hold' 
		});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(table, 'approval');
  }
};
