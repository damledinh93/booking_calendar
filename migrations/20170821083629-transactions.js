'use strict';

const table = 'Transactions';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(table, {
            id: { type: Sequelize.STRING, allowNull: false, unique: true, primaryKey: true },
            fromUserId: {
                type: Sequelize.STRING,
                references: {
                    model: 'Users',
                    key: 'id'
                }
            },
            toUserId: {
                type: Sequelize.STRING,
                references: {
                    model: 'Users',
                    key: 'id'
                }
            },
            sellRequestId: {
                type: Sequelize.STRING,
                references: {// One : Oneyes
                    model: 'SellRequests',
                    key: 'id'
                }
            },
            buyRequestId: {
                type: Sequelize.STRING,
                references: {
                    model: 'BuyRequests',
                    key: 'id'
                }
            },
            withdrawRequestId: {
                type: Sequelize.STRING,
                references: {
                    model: 'WithDrawRequests',
                    key: 'id'
                }
            },
            incomingRequestId: {
                type: Sequelize.STRING,
                references: {
                    model: 'IncomingRequests',
                    key: 'id'
                }
            },
            coin: { type: Sequelize.STRING, allowNull: false },
            amount: { type: Sequelize.STRING(32), allowNull: false },
            fee: { type: Sequelize.STRING(32), allowNull: false },
            createdAt: { type: Sequelize.DATE },
            updatedAt: { type: Sequelize.DATE, defaultValue: new Date() }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable(table);
    }
};
