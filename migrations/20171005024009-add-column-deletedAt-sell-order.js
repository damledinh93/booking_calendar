'use strict';

const table = 'SellOrders';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.addColumn(table, 'deletedAt', {
			type: Sequelize.DATE, defaultValue: null 
		});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.removeColumn(table, 'deletedAt');
	}
};
