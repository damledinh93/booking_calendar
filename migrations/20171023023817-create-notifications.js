'use strict';

const table = 'Notifications';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable(table, {
			id: { type: Sequelize.STRING, allowNull: false, unique: true, primaryKey: true },
			userId: {
                type: Sequelize.STRING,
                references: {
                    model: 'Users',
                    key: 'id'
                }
			},
			metadata: { type: Sequelize.JSON },
			isRead: { type: Sequelize.STRING, defaultValue: false },
			actionType: { type: Sequelize.INTEGER },
            createdAt: { type: Sequelize.DATE, defaultValue: new Date() },
            updatedAt: { type: Sequelize.DATE, defaultValue: new Date() }
        });
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable(table);
	}
};
