'use strict';

const table = 'Managements';
module.exports = {
	up: function (queryInterface, Sequelize) {
		return queryInterface.createTable(table, {
			id: { type: Sequelize.STRING, allowNull: false, unique: true, primaryKey: true },
			coin: { type: Sequelize.STRING },
			action: { type: Sequelize.STRING },
			type: { type: Sequelize.STRING },
			amount: { type: Sequelize.STRING(32) },
			percent: { type: Sequelize.STRING(10) },
			actionCurrency:  { type: Sequelize.STRING },
			updatedAt: { type: Sequelize.DATE, defaultValue: new Date() },
			createdAt: { type: Sequelize.DATE, defaultValue: new Date() }
		});
	},

	down: function (queryInterface, Sequelize) {
		return queryInterface.dropTable(table);
	}
};
