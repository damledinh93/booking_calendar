'use strict';

const tableName = 'Tickets';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable(tableName, { 
			id: { type: Sequelize.STRING, primaryKey: true, unique: true },
			status: {type: Sequelize.STRING, defaultValue: 'open'},
			buyRequestId: {
                type: Sequelize.STRING,
                references: {
                    model: 'BuyRequests',
                    key: 'id'
                }
			},
			sellRequestId: {
                type: Sequelize.STRING,
                references: {
                    model: 'SellRequests',
                    key: 'id'
                }
			},
			userId: {
				type: Sequelize.STRING,
				references: {
					model: 'Users',
					key: 'id'
				}
			},
			createdAt: { type: Sequelize.DATE },
			updatedAt: { type: Sequelize.DATE }
		});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable(tableName);
	}
};
