'use strict';

const table = 'Users';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.addColumn(table, 'status', {
			type: Sequelize.STRING, defaultValue: 'active' 
		});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.removeColumn(table, 'status');
	}
};
