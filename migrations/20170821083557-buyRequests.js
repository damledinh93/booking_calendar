'use strict';

const table = 'BuyRequests';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(table, {
            id: { type: Sequelize.STRING, allowNull: false, unique: true, primaryKey: true },
            status: { type: Sequelize.STRING, allowNull: false, default: 'pending' },
            buyOrderId: {
                type: Sequelize.STRING,
                references: {
                    model: 'BuyOrders',
                    key: 'id'
                }
            },
            ownerId: {
                type: Sequelize.STRING,
                references: {
                    model: 'Users',
                    key: 'id'
                }
            },
            buyerId: {
                type: Sequelize.STRING,
                references: {
                    model: 'Users',
                    key: 'id'
                }
            },
            coinAmount: { type: Sequelize.STRING(32), allowNull: false },
            coinCurrency: { type: Sequelize.STRING, allowNull: false },
            moneyAmount: { type: Sequelize.STRING(32), allowNull: false },
            moneyCurrency: { type: Sequelize.STRING, allowNull: false },
            fee: { type: Sequelize.STRING(32), allowNull: false },
            expireAt: { type: Sequelize.DATE, allowNull: false},
            createdAt: { type: Sequelize.DATE },
            updatedAt: { type: Sequelize.DATE }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable(table);
    }
};
