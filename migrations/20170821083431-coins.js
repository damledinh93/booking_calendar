'use strict';

const table = 'Coins';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(table, {
            id: { type: Sequelize.STRING, allowNull: false, unique: true, primaryKey: true },
            exchange: {type: Sequelize.STRING, allowNull: false},
            type: {type: Sequelize.STRING, allowNull: false},
            currency: {type: Sequelize.STRING, defaultValue: 'usd'},
            last: {type: Sequelize.STRING(32), allowNull: false},
            timestamp: {type: Sequelize.BIGINT, defaultValue: new Date().getTime()},
            high: {type: Sequelize.STRING(32)},
            low: {type: Sequelize.STRING(32)},
            bid: {type: Sequelize.STRING(32)},
            ask: {type: Sequelize.STRING(32)},
            createdAt: { type: Sequelize.DATE, defaultValue: new Date() },
            updatedAt: { type: Sequelize.DATE, defaultValue: new Date() }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable(table);
    }
};

