'use strict';

const table = 'Exchanges';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(table, {
            id: { type: Sequelize.STRING, allowNull: false, unique: true, primaryKey: true },
            shortName: { type: Sequelize.STRING, allowNull: false },
            fullName: { type: Sequelize.STRING, allowNull: false },
            coinsSupport: { type: Sequelize.ARRAY(Sequelize.JSON) },
            suffix: { type: Sequelize.STRING, allowNull: false },
            url: { type: Sequelize.STRING, allowNull: false },
            createdAt: { type: Sequelize.DATE, defaultValue: new Date() },
            updatedAt: { type: Sequelize.DATE, defaultValue: new Date() }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable(table);
    }
};

