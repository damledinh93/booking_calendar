'use strict';

const table = 'Confirms';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(table, {
            id: { type: Sequelize.STRING, allowNull: false, unique: true, primaryKey: true },
            userId: {
                type: Sequelize.STRING,
                references: {
                    model: 'Users',
                    key: 'id'
                }
            },
            buyRequestId: {
                type: Sequelize.STRING,
                references: {
                    model: 'BuyRequests',
                    key: 'id'
                }
            },
            sellRequestId: {
                type: Sequelize.STRING,
                references: {
                    model: 'SellRequests',
                    key: 'id'
                }
            },
            withdrawRequestId: {
                type: Sequelize.STRING,
                references: {
                    model: 'WithDrawRequests',
                    key: 'id'
                }
            },
            createdAt: { type: Sequelize.DATE, defaultValue: new Date() },
            updatedAt: { type: Sequelize.DATE, defaultValue: new Date() }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable(table);
    }
};
